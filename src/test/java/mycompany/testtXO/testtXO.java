/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package mycompany.testtXO;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author asus
 */
public class testtXO {
    
    public testtXO() {
    }

    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    // TDD Test Driven Development
    @Test
    public void testCheckWinRow1_X_output_true(){
        char[][] table = {{'X','X','X'},
                          {'-','-','-'},
                          {'-','-','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinRow2_X_output_true(){
        char[][] table = {{'-','-','-'},
                          {'X','X','X'},
                          {'-','-','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinRow3_X_output_true(){
        char[][] table = {{'-','-','-'},
                          {'-','-','-'},
                          {'X','X','X'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    
    
    @Test
    public void testCheckWinRow1_X_output_false(){
        char[][] table = {{'X','O','X'},
                          {'-','-','-'},
                          {'-','-','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWinRow2_X_output_false(){
        char[][] table = {{'-','-','-'},
                          {'X','-','X'},
                          {'-','O','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWinRow3_X_output_false(){
        char[][] table = {{'-','-','-'},
                          {'-','-','-'},
                          {'X','X','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    
//    
//    @Test
//    public void testCheckWinRow1_O_output_true(){
//        char[][] table = {{'O','O','O'},
//                          {'-','-','-'},
//                          {'-','-','-'}};
//        char player = 'O';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    @Test
//    public void testCheckWinRow2_O_output_true(){
//        char[][] table = {{'-','-','-'},
//                          {'O','O','O'},
//                          {'-','-','-'}};
//        char player = 'O';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    @Test
//    public void testCheckWinRow3_O_output_true(){
//        char[][] table = {{'-','-','-'},
//                          {'-','-','-'},
//                          {'O','O','O'}};
//        char player = 'O';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
    
    @Test
    public void testCheckWinCol1_O_output_true(){
        char[][] table = {{'O','-','-'},
                          {'O','-','-'},
                          {'O','-','-'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    public void testCheckWinCol2_O_output_true(){
        char[][] table = {{'-','O','-'},
                          {'-','O','-'},
                          {'-','O','-'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinCol3_O_output_true(){
        char[][] table = {{'-','-','O'},
                          {'-','-','O'},
                          {'-','-','O'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    
    
    @Test
    public void testCheckWinCol1_O_output_false(){
        char[][] table = {{'O','-','-'},
                          {'O','X','-'},
                          {'-','-','-'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    public void testCheckWinCol2_O_output_false(){
        char[][] table = {{'-','-','-'},
                          {'-','O','-'},
                          {'-','O','X'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWinCol3_O_output_false(){
        char[][] table = {{'-','-','X'},
                          {'-','-','O'},
                          {'-','-','O'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    
//    @Test
//    public void testCheckWinCol1_X_output_true(){
//        char[][] table = {{'X','-','-'},
//                          {'X','-','-'},
//                          {'X','-','-'}};
//        char player = 'X';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    @Test
//    public void testCheckWinCol2_X_output_true(){
//        char[][] table = {{'-','X','-'},
//                          {'-','X','-'},
//                          {'-','X','-'}};
//        char player = 'X';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    @Test
//    public void testCheckWinCol3_X_output_true(){
//        char[][] table = {{'-','-','X'},
//                          {'-','-','X'},
//                          {'-','-','X'}};
//        char player = 'X';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    
  
    
   // @Test
    //public void testAdd_num1_2_num2_2_output_4(){
      //  int result = Calculation.add(2,2);
        //assertEquals(4,result);
//}
  //  @Test
    //public void testAdd_num1_3_num2_2_output_5(){
      //  int result = Calculation.add(3,2);
        //assertEquals(5,result);
//}
  //  @Test
    //public void testAdd_num1_100_num2_20_output_120(){
      //  int result = Calculation.add(100,20);
        //assertEquals(120,result);
//}
//}
     public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow2_O_output_true (){
        String[][] table = {{"1","2","3"},{"O","O","O"},{"7","8","9"}};
        String player = "O";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinRow1_O_output_true (){
        String[][] table = {{"O","O","O"},{"4","5","6"},{"7","8","9"}};
        String player = "O";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    
    @Test
    public void testCheckWinRow3_O_output_true (){
        String[][] table = {{"1","2","3"},{"4","5","6"},{"O","O","O"}};
        String player = "O";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    
    @Test
    public void testCheckWinRow3_O_output_false (){
        String[][] table = {{"1","2","3"},{"4","5","6"},{"O","O","9"}};
        String player = "O";
        boolean result = OX.checkWin(table,player);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckWinCol1_X_output_false (){
        String[][] table = {{"X","2","3"},{"X","5","6"},{"X","8","9"}};
        String player = "O";
        boolean result = OX.checkWin(table,player);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckWinCol1T_X_output_true (){
        String[][] table = {{"X","2","3"},{"X","5","6"},{"X","8","9"}};
        String player = "X";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinCol3_X_output_true (){
        String[][] table = {{"1","2","X"},{"4","5","X"},{"7","8","X"}};
        String player = "X";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinCol2_O_output_true (){
        String[][] table = {{"1","O","3"},{"4","O","6"},{"7","O","9"}};
        String player = "O";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinDi_X_output_true (){
        String[][] table = {{"1","2","X"},{"4","X","6"},{"X","8","9"}};
        String player = "X";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinDi1_X_output_true (){
        String[][] table = {{"X","2","3"},{"4","X","6"},{"7","8","X"}};
        String player = "X";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWinDi2_X_output_false (){
        String[][] table = {{"X","2","3"},{"4","X","6"},{"7","X","9"}};
        String player = "X";
        boolean result = OX.checkWin(table,player);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckDraw_X_output_false (){
        String[][] table = {{"X","X","X"},{"X","X","X"},{"X","X","X"}};
        String player = "X";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckDraw_OX_output_false (){
        String[][] table = {{"O","X","X"},{"X","X","X"},{"X","X","X"}};
        String player = "X";
        boolean result = OX.checkWin(table,player);
        assertEquals(true,result);
    }
}